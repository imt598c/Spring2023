data=[['SOAR TECHNOLOGY INC', 18, 2884411.0],
 ['SPECTRAL ENERGIES LLC', 19, 349634.0],
 ['ALPHACORE INC', 23, 199922.0],
 ['Space Micro Inc.', 12, 399249.69],
 ['Arete Associates', 24, 154891.0],
 ['APTIMA INC', 13, 1499341.9],
 ['INTELLISENSE SYSTEMS INC', 39, 3826758.04],
 ['LYNNTECH INC', 27, 1023849.0],
 ['TOYON RESEARCH CORPORATION', 26, 649999.0],
 ['Nou Systems, Inc.', 12, 464987.0],
 ['SCIENTIFIC SYSTEMS CO INC', 18, 149994.0],
 ['Radiation Monitoring Devices, Inc.', 21, 1485777.0],
 ['Stottler Henke Associates, Inc.', 13, 139946.0]]

 import matplotlib.pyplot as plt

 x_data = [company_awards[row] for row in company_awards]
y_data = [company_funding[row] for row in company_funding]

x_data = [row[1] for row in data]
y_data = [row[2] for row in data]

# Define the labels
x_label = "Number of Awards"
y_label = "Dollar Amount"

# Define the title
title = "Awarded Contracts"

# Plot the data as a scatter plot
plt.scatter(x_data, y_data)
plt.xlabel(x_label)
plt.ylabel(y_label)
plt.title(title)

# Add labels to each point
for i in range(len(data)):
    #plt.annotate(i, xy=(company_awards[i], company_funding[i]), xytext=(5, 5), textcoords="offset points")
    plt.annotate(''.join(data[i][0]), xy=(data[i][1], data[i][2]), xytext=(5, 5), textcoords="offset points")

# Show the plot
plt.show()